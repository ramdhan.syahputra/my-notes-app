//
//  AddView.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 27/11/23.
//

import SwiftUI
import SwiftData

struct AddView: View {
    @State private var title = ""
    @State private var content = ""
    @State private var isError = false
    @Environment(\.dismiss) private var dismiss
    @Environment(\.modelContext) private var context
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Title", text: $title)
                    .autocorrectionDisabled()
                TextField("Content", text: $content, axis: .vertical)
                    .lineLimit(5, reservesSpace: true)
                    .autocorrectionDisabled()
                
                Section() {
                    Button("Create") {
                        if title.isEmpty || content.isEmpty {
                            isError = true
                            return
                        }
                        
                        let note = Note(id: UUID().uuidString, title: title, content: content)
                        context.insert(note)
                        try? context.save()
                        dismiss()
                    }.frame(maxWidth: .infinity)
                    
                    Button("Reset", role: .destructive) {
                        title = ""
                        content = ""
                    }.frame(maxWidth: .infinity)
                }
            }
            .alert("Warning", isPresented: $isError) {
                Button("Try again") {
                    
                }
            } message: {
                Text("Please enter your title and content the first!")
            }
            .toolbar {
                ToolbarItem {
                    Button("Close") {
                        dismiss()
                    }
                }
            }
            .navigationTitle("Add note")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#Preview {
    AddView()
}
