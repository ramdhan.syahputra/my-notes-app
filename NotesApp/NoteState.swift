//
//  NoteState.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 15/12/23.
//

import Foundation

@Observable
class NoteState {
    var isError = false
    var isEdited = false
    var query = ""
    var isSheetShowed = false
    var editedNote: Note?
}
