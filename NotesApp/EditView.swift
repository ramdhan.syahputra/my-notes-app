//
//  EditView.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 27/11/23.
//

import SwiftUI

struct EditView: View {
    @Bindable var note: Note
    private var title: String = ""
    private var content: String = ""
    @State private var isError = false
    @Environment(\.dismiss) private var dismiss
    @Environment(\.modelContext) private var context
    
    init(note: Note) {
        self.note = note
        self.title = note.title
        self.content = note.content
    }
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Title", text: $note.title)
                TextField("Content", text: $note.content, axis: .vertical)
                    .lineLimit(5, reservesSpace: true)
                
                Section {
                    Button("Update") {
                        if note.title.isEmpty || note.content.isEmpty {
                            isError = true
                            note.title = title
                            note.content = content
                            return
                        }
                        
                        dismiss()
                    }.frame(maxWidth: .infinity)
                    
                    Button("Delete", role: .destructive) {
                        context.delete(note)
                        try? context.save()
                        dismiss()
                    }.frame(maxWidth: .infinity)
                }
            }
            .alert("Warning", isPresented: $isError) {
                Button("Try again") {
                    
                }
            } message: {
                Text("Please enter your title and content the first!")
            }
            .toolbar {
                ToolbarItem {
                    Button("Close") {
                        dismiss()
                    }
                }
            }
            .navigationTitle("Update note")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#Preview {
    EditView(note: Note(id: "", title: "", content: ""))
}
