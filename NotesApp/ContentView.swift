//
//  ContentView.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 25/11/23.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    @State var noteState: NoteState = NoteState()
    @Environment(\.modelContext) private var context
    @Query(sort: \Note.createdAt, order: .reverse, animation: .spring(.bouncy)) var allNotes: [Note]
    
    var body: some View {
        NavigationStack {
            VStack {
                if allNotes.isEmpty {
                    ContentUnavailableView("Empty notes", systemImage: "folder.badge.questionmark")
                } else {
                    List {
                        ForEach(allNotes) { note in
                            NoteCard(note: note)
                                .onTapGesture {
                                    noteState.editedNote = note
                                }
                        }
                        .onDelete(perform: deleteNote)
                    }
                }
            }
            .navigationTitle("My Notes")
            .alert(isPresented: $noteState.isError, content: {
                Alert(title: Text("Warning"), message: Text("Please type the title or content first!"), dismissButton: .cancel(Text("OK")))
            })
            .searchable(text: $noteState.query)
            .sheet(isPresented: $noteState.isSheetShowed) {
                AddView()
                    .presentationDetents([.medium])
            }
            .sheet(item: $noteState.editedNote) {
                noteState.editedNote = nil
            } content: { note in
                EditView(note: note)
            }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button {
                        noteState.isSheetShowed = !noteState.isSheetShowed
                    } label: {
                        Image(systemName: "plus.circle")
                    }
                }
                
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        
                    } label: {
                        EditButton()
                    }
                }
            }
        }
    }
    
    func deleteNote(_ indexSet: IndexSet) {
        for index in indexSet {
            context.delete(allNotes[index])
        }
        
        try? context.save()
    }
}

#Preview {
    ContentView().modelContainer(for: Note.self)
}

struct NoteCard: View {
    var note: Note
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(note.title)
                    .font(.headline.bold())
                    .lineLimit(1)
                Spacer()
                Text(note.createdAt.formatted(date: .omitted, time: .shortened))
            }
            Text(note.content)
                .font(.body)
        }
    }
}
