//
//  Note.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 27/11/23.
//

import Foundation
import SwiftData

@Model
class Note: Identifiable {
    @Attribute(.unique) var id: String
    var title: String
    var content: String
    var createdAt: Date
    
    init(id: String, title: String, content: String) {
        self.id = id
        self.title = title
        self.content = content
        self.createdAt = Date()
    }
}
