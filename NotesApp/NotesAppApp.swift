//
//  NotesAppApp.swift
//  NotesApp
//
//  Created by M Ramdhan Syahputra on 25/11/23.
//

import SwiftUI
import SwiftData

@main
struct NotesAppApp: App {
    let modelContainer: ModelContainer
        
    init() {
        do {
            modelContainer = try ModelContainer(for: Note.self)
        } catch {
            fatalError("Could not initialize ModelContainer")
        }
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .modelContainer(modelContainer)
        }
    }
}
